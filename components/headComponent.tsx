// react
import Head from 'next/head';
import React from 'react';
// Lib
import { GetSeo } from '../lib/request';
import { GetImg } from '../lib/utils';
// Component
import LoaderComponent from './loaderComponent';

export default function HeadComponent() {

  const { loading, error, data } = GetSeo()
  let title = '';
  let description = '';
  let icon = '';


  if(loading) {
    return <LoaderComponent />
  } else {

    if (data) {
      const source = data.seo.data.attributes;

      title = source.title;
      description = source.description;
      icon = source.icon.data.attributes.url;
    }

    return (
      <>
        <Head>
          <meta name="viewport" content="width=device-width, initial-scale=1" />
          <meta charSet="utf-8" />
          <meta itemProp="description" content={description} />
          <title>{title}</title>

          <meta itemProp="name" content={title} />
          <meta itemProp="image" content={icon} />

          <meta property="og:title" content={title} key="ogtitle" />
          <meta property="og:description" content={description} key="ogdesc" />
          <meta property="og:site_name" content={"Rock'n Wood"} key="ogsitename" />
          <meta property="og:url" content={'flopinou.kvly.fr'} />

          <meta name="twitter:card" content="summary" />
          <meta name="twitter:site" content="@flickr" />
          <meta name="twitter:title" content="Small Island Developing States Photo Submission" />
          <meta name="twitter:description" content="View the album on Flickr." />
          <meta name="twitter:image" content="https://farm6.staticflickr.com/5510/14338202952_93595258ff_z.jpg" />

          <link rel="icon" href={`${GetImg(icon)}`} type="image/x-icon"/>
          <link rel="icon" href={icon} type="image/x-icon"/>
        </Head>
      </>

    )
  }

}
