// react
import Head from 'next/head';
import React from 'react';
// Lib
import { GetImg } from '../lib/utils';
// Component

export default function HeaderComponent(props: any) {

  const title = props.title;
  const description = props.description;
  const img = GetImg(props.img);
  const icon = props.icon || '/uploads/bat_e6913fe77b.png';

    return (
      <>
        <Head>
          <meta name="viewport" content="width=device-width, initial-scale=1" />
          <meta charSet="utf-8" />
          <meta itemProp="description" content={description} />
          <title>{title}</title>

          <meta itemProp="name" content={title} />
          <meta itemProp="image" content={img} />

          <meta property="og:title" content={title} key="ogtitle" />
          <meta property="og:description" content={description} key="ogdesc" />
          <meta property="og:site_name" content={"Rock'n Wood"} key="ogsitename" />
          <meta property="og:url" content={'flopinou.kvly.fr'} />
          <meta property="og:image" content={img} />

          <meta name="twitter:card" content="summary" />
          <meta name="twitter:site" content="rock'n wood" />
          <meta name="twitter:title" content={title} />
          <meta name="twitter:description" content={description} />
          <meta name="twitter:image" content={img} />

          <link rel="icon" href={`${GetImg(icon)}`} type="image/x-icon"/>
          <link rel="icon" href={icon} type="image/x-icon"/>
        </Head>
      </>

    )
  // }

}
