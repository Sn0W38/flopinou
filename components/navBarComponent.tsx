// React
import React, { useState } from 'react';
import Image from 'next/image';
import Link from 'next/link';
// Lib
import { GetLogo } from '../lib/request';
// Component
import LoaderComponent from './loaderComponent';

export default function NavBarComponent() {

  const { loading, error, data } = GetLogo()
  const [isActive, setIsActive] = useState(false)
  const [isDropActive, setIsDropActive] = useState(false)

  if (loading) {
    return <LoaderComponent />
  } else {
    const title = data.logo.data.attributes.title;
    const logo = data.logo.data.attributes.icon.data.attributes.url;
    const width = data.logo.data.attributes.icon.data.attributes.width;
    const height = data.logo.data.attributes.icon.data.attributes.height;

    return (
      <>
        <nav className={'navbar mr-2 ml-2 p-2"'} role='navigation' aria-label='main navigation'>
          <div className='navbar-brand'>
            {/*Icon*/}
            <Link className={'navbar-item'} href="/">
              <span className={'logo'}>
                <Image src={logo}
                       alt={title}
                       width={width}
                       height={height}
                />
              </span>
            </Link>

            {/*Hamburger*/}
            <a
              onClick={() => {
                setIsActive(!isActive)
              }}
              role='button'
              className={`navbar-burger burger ${isActive ? 'is-active' : ''}`}
              aria-label='menu'
              aria-expanded='false'
              data-target='navbarBasicExample'
            >
              <span aria-hidden='true'></span>
              <span aria-hidden='true'></span>
              <span aria-hidden='true'></span>
            </a>
          </div>

        <div id="navbarExampleTransparentExample" className={`navbar-menu ${isActive ? 'is-active' : ''}`}>
          {/*Navbar gauche*/}
          <div className="navbar-start">
            {/*Btn classique*/}
            <span className="navbar-item">
              <Link href={'/'}>Home</Link>
            </span>
            {/*Dropdown*/}
            <div  className={`navbar-item has-dropdown is-hoverable ${isDropActive ? 'is-active' : ''}`}
                  onClick={() => {
                    setIsDropActive(!isDropActive)
                  }}
            >
              <span className="navbar-link">
                Docs
              </span>
              <div className="navbar-dropdown is-boxed">
                <span className="navbar-item">
                  <Link href={'/'}>Overview</Link>
                </span>
                <span className="navbar-item">
                  <Link href={'/'}>Modifiers</Link>
                </span>
                <span className="navbar-item">
                  <Link href={'/'}>Columns</Link>
                </span>
                <span className="navbar-item">
                  <Link href={'/'}>Layout</Link>
                </span>
                <span className="navbar-item">
                  <Link href={'/'}>Form</Link>
                </span>
              </div>
            </div>
          </div>
        </div>

        {/*Navbar droite*/}
        <div id='navbarBasicExample' className={`navbar-menu ${isActive ? 'is-active' : ''}`}>
          <div className='navbar-end'>
            <div className='navbar-item'>
              <Link href='/' className='navbar-item'>
                <button className={'button is-link'}>Contact</button>
              </Link>
            </div>
          </div>
        </div>
        </nav>
      </>

    )
  }
}
