// REACT BASIC
import React from 'react';
import Image from 'next/image';
//LIB
import Link from 'next/link';

export default function ProjetComponent(props: any) {

  return (
    <>
      <div className="card">
        <div className="card-image">
          <figure className="image is-5by3">
            <Image  src={`${props?.after?.data?.attributes?.url}`}
                    alt={`${props?.after?.data?.attributes?.url}`}
                    layout='fill'
                    priority={true}
            />
          </figure>
        </div>

        <div className="card-content">
          <p className='title'>{props.title}</p>
          <div className={'subtitle wrap-word'}>
            <p className='word'>{props.content}</p>
          </div>

          <Link href={`/projets/${props.title}`}>
            <a className="button is-primary">
              Voir plus
            </a>
          </Link>
        </div>
      </div>
    </>

  )
}
