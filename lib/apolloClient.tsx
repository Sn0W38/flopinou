import { ApolloClient, ApolloLink, createHttpLink, InMemoryCache } from '@apollo/client';

// URL DE L'API
const link: ApolloLink = createHttpLink({
  uri: process.env.API,
  headers: {
    "Authorization": "Bearer b5132a14f0a157c35fe003429f4166b06b09455d2b7afaa75d696a2c4f79fb390db8c7ca06f360f90cbdda3d5fc7001fb30e0d920d576a98fcbe8abe349b32a3fd3f2051a91db502f4a3992a57b3c1576dd26cf8b57bcebdac7ed2ad29320868bb03c8d534724db91f3f9a283942aca7d94d1c406763b8b715a8002dbe0aa356"
  },
  fetch: fetch
});

/**
 * cache-first : c'est la valeur par défaut, le client recherche le résultat dans le cache avant de faire une requête.
 * cache-and-network : le client retournera le contenu du cache, mais fera tout de même la requête afin de le mettre à jour, permet d'avoir une réponse rapide.
 * network-only : le client ne retournera jamais le contenu du cache pour cette requête et fera systématiquement un appel réseau.
 * cache-only : le client ne fera aucun appel réseau et se contentera de lire le cache.
 * no-cache : le client fera un appel réseau et ne lira pas le cache, mais au contraire de network-only, le résultat de la requête ne sera pas écrit dans le cache.
 */
const client = new ApolloClient({
  ssrMode: typeof window === 'undefined',
  link,
  cache: new InMemoryCache(),
  defaultOptions: {
    query: {
      fetchPolicy: 'no-cache',
    }
  }
});

export default client;
