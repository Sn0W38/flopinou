// REACT BASIC
import { DocumentNode, gql, useQuery } from '@apollo/client';
// LIB
import apolloClient from './apolloClient';

// =================================================
// Get init data

const GET_INIT: DocumentNode = gql`
  query init {
    projets {
      data {
      id,
      attributes {
        title,
        content,
        after {
          data {
            attributes {
              url
            }
          }
        }
      }
    }
  },
  seo {
    data {
      attributes {
        title,
        description,
        icon {
          data {
            attributes {
              url
            }
          }
        }
      }
    }
  }
}
`;

export function GetInit() {
  return apolloClient.query({
    query: GET_INIT
  })
}

// =================================================
// Get Projet by ID
// Static build limit 10 projets
const GET_ALL_PROJET: DocumentNode = gql`
query getProject {
  projets(pagination: {limit: 10}) {
    data {
      id,
      attributes {
        title,
        content,
        before {
          data {
            attributes {
              url
            }
          }
        },
        after {
          data {
            attributes {
              url
            }
          }
        }
      }
    }
  }
}
`;

export function GetAllProjet() {
  return apolloClient.query({
    query: GET_ALL_PROJET
  })
}

// =================================================
// Get Projet by ID
const GET_PROJET: DocumentNode = gql`
query GetProjetsBySlug($filters: ProjetFiltersInput) {
  projets(filters: $filters) {
    data {
      id,
      attributes {
        title,
        content,
        before {
          data {
            attributes {
              url
            }
          }
        },
        after {
          data {
            attributes {
              url
            }
          }
        }
      }
    }
  }
}
`;

export function GetProjet(slug: string) {
  return apolloClient.query({
    query: GET_PROJET,
    variables: {
      filters: {
        title: {
          eq: slug
        }
      }
    }
  });
}

// =================================================
// Get Metadata for SEA
const GET_SEO: DocumentNode = gql`
query getSeo {
  seo {
    data {
      attributes {
        title,
        description,
        icon {
          data {
            attributes {
              url
            }
          }
        }
      }
    }
  }
}
`;
export function GetSeo() {
  return useQuery(GET_SEO)
}

// =================================================
// Get Logo base

const GET_LOGO = gql`
query getLogoSource {
  logo {
    data {
      attributes {
        title,
        icon {
          data {
            attributes {
              url,
              width,
              height
            }
          }
        }
      }
    }
  }
}
`;

export function GetLogo() {
  return useQuery(GET_LOGO);
}

