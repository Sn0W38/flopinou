/**
 * Retourne le nom de domain principal de l'API
 * @returns {string}
 */
export function getEnvUrl(): string {
  return process.env.FRONT ? process.env.FRONT : '';
}

/**
 * Retourne le lien source de l'image
 * @param src: string
 * @return url de l'image
 */
export function GetImg(src: string): string {
  return process.env.FRONT + src;
}
