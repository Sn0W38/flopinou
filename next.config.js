/** @type {import('next').NextConfig} */
var path = require("path");

const nextConfig = {
  reactStrictMode: true,
  sassOptions: {
    includePaths: [path.join(__dirname, 'styles')]
  },
  images: {
    loader: 'imgix',
    path: process.env.FRONT,
    basePath: '/',
    disableStaticImages: false,
    domains: ['localhost', 'apirocknwood.kvly.fr', 'rocknwood.kvly.fr']
  },
  env: {
    API: process.env.API,
    TOKEN: process.env.TOKEN,
    FRONT: process.env.FRONT,
    EMAIL: process.env.EMAIL,
    PASSWORD: process.env.PASSWORD
  }
}

module.exports = nextConfig
