// CSS
import '../styles/globals.scss'
// REACT BASIC
import React from 'react';
import type { AppProps } from 'next/app'
import { ApolloProvider } from '@apollo/client';
//COMPONENTS
import HeadComponent from '../components/headComponent';
import NavBarComponent from '../components/navBarComponent';

// LIB
import client from '../lib/apolloClient';

export default function App({ Component, pageProps }: AppProps) {
  return (
    <ApolloProvider client={client}>
    {/*Init Graphql client*/}

      <HeadComponent />
      {/*Métadonnée du site*/}

      <NavBarComponent />
      {/*Bar de navigation*/}

      <Component {...pageProps} />
      {/*Métadonnée du site*/}

    </ApolloProvider>
  )
}
