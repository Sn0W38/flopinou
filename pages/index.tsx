// REACT BASIC
import React  from 'react';
import { ApolloQueryResult } from '@apollo/client';
// LIB
import { GetInit } from '../lib/request';
// Component
import ProjetComponent from '../components/projetComponent';
import LoaderComponent from '../components/loaderComponent';
import HeaderComponent from '../components/headerComponent';

export default function Index(props:any) {

  const projets = props.projets;
  const loading = props.loading;
  const seo = props.seo;

  const listProjet = projets.map((projets: any) => (
    <div key={projets.id} className='column is-4-desktop is-6-tablet is-12-mobile'>
      <ProjetComponent
        id={projets.id}
        title={projets.attributes.title}
        content={projets.attributes.content}
        after={projets.attributes.after} />
    </div>
  ));


  if(loading) {
    return <LoaderComponent />
  } else {
    return (
      <>
        <HeaderComponent title={seo.title}
                         img={projets[0].attributes.after.data.attributes.url}
                         description={seo.description}
                         icon={seo.icon.data.attributes.url}
        />
        {/*Métadonnée du site*/}
        <div className='columns is-multiline m-2'>
          {listProjet}
        </div>
      </>
    )
  }
}


export async function getStaticProps() {
  const data: ApolloQueryResult<any> = await GetInit()

  return {
    props: {
      projets: data.data.projets.data,
      seo: data.data.seo.data.attributes,
      loading: data.loading,
      networkStatus: data.networkStatus
    },
    revalidate: 60
  }
}
