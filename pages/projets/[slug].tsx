// React
import React from 'react';
import DefaultErrorPage from 'next/error'
// React Image comparaison
import ReactCompareImage from 'react-compare-image';
// Lib
import { GetImg } from '../../lib/utils';
import { GetAllProjet, GetProjet } from '../../lib/request';
// Component
import LoaderComponent from '../../components/loaderComponent';
import HeaderComponent from '../../components/headerComponent';

export default function Projet(props: any) {

  const projet = props?.data?.projets?.data[0]?.attributes;

  if (projet) {
    return (
      <>
        <HeaderComponent  title={projet.title}
                          description={projet.content}
                          img={projet?.after?.data?.attributes?.url}
                          icon={''}
        />

        <div className={'container columns is-multiline m-2 p-2 has-text-centered'}>
          <p className={'column is-12 title'}>
            projet: {projet.title}
          </p>

          <div className={'column is-8-desktop is-10-tablet is-12-mobile card-image m-auto'}>
            <ReactCompareImage leftImage={`${GetImg(projet?.before?.data?.attributes?.url)}`}
                               rightImage={`${GetImg(projet?.after?.data?.attributes?.url)}`}
                               leftImageLabel="Avant"
                               rightImageLabel="Après"
                               skeleton={<LoaderComponent />}
            />
          </div>

          <div className={'column is-12 has-text-justified'}>
            {projet.content}
          </div>

        </div>

      </>
    )
  } else {
    return (
      // TODO 404
      <>
        <DefaultErrorPage  statusCode={404}/>
      </>
    )
  }

}

export async function getStaticPaths() {

  const { data } = await GetAllProjet();

  const paths = data.projets.data.map((prj: any) => {

    return {
      params: {
        slug: `${prj.attributes.title}`
      }
    }
  });

  return {
    paths,
    fallback: true
  }
}


export async function getStaticProps(context: any) {
  const id = context.params.slug;
  const { data } = await GetProjet(id);

  return {
    props: {
      data
    },
    revalidate: 60
  }
}
